package com.farid.testappapi.network.apiGitHub

import androidx.lifecycle.LiveData
import com.farid.testappapi.models.response.ListAllPublicResponse
import com.farid.testappapi.models.response.infoRepo.InfoRepoResponse
import com.farid.testappapi.network.BaseRepository

class ListApiRepository(listApiInterface: ListApiInterface) :
    BaseRepository<ListApiInterface>(listApiInterface) {

    fun getList(lastId: Int): LiveData<ArrayList<ListAllPublicResponse>> {
        return fetchData {
            service.getAllListPublicRepo(lastId)
        }
    }
    fun getRepo(owner:String,title:String): LiveData<InfoRepoResponse> {
        return fetchData {
            service.getRepo(owner,title)
        }
    }
}