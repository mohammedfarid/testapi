package com.farid.testappapi.network.apiGitHub

import com.farid.testappapi.BuildConfig
import com.farid.testappapi.models.response.ListAllPublicResponse
import com.farid.testappapi.models.response.infoRepo.InfoRepoResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ListApiInterface {
    @GET("/repositories")
    fun getAllListPublicRepo(@Query("since") lastId: Int): Deferred<Response<ArrayList<ListAllPublicResponse>>>

    @GET("/repos/{owner}/{title}")
    fun getRepo(@Path("owner") owner: String, @Path("title") title: String): Deferred<Response<InfoRepoResponse>>

    companion object {
        fun create(): ListApiInterface {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
            return Retrofit.Builder()
                .baseUrl(BuildConfig.TEST_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(client)
                .build()
                .create(ListApiInterface::class.java)
        }
    }
}