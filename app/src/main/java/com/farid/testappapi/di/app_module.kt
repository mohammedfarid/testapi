package com.farid.testappapi.di

import com.farid.testappapi.network.apiGitHub.ListApiInterface
import com.farid.testappapi.network.apiGitHub.ListApiRepository
import com.farid.testappapi.ui.lists.ListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val apiModule = module {
    single { ListApiInterface.create() }
    single { ListApiRepository(get()) }
    viewModel { ListViewModel(get()) }
}