package com.farid.testappapi.ui.lists


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.farid.testappapi.R
import kotlinx.android.synthetic.main.fragment_web_view.*
import kotlinx.android.synthetic.main.fragment_web_view.view.*


/**
 * A simple [Fragment] subclass.
 */
class WebViewFragment : Fragment() {
    companion object {
        var TAG = WebViewFragment::class.java.simpleName
        const val KET_URL = "url"

        fun newInstance(url: String) = run {
            val fragment = WebViewFragment()
            fragment.arguments = bundleOf(
                KET_URL to url
            )
            return@run fragment
        }
    }

    lateinit var url: String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fetchBundle(arguments)
        var v = inflater.inflate(R.layout.fragment_web_view, container, false)
        v.webview.loadUrl(url)

        // Enable Javascript
        // Enable Javascript
        val webSettings: WebSettings = v.webview.settings
        webSettings.javaScriptEnabled = true

        // Force links and redirects to open in the WebView instead of in a browser
        // Force links and redirects to open in the WebView instead of in a browser
        v.webview.webViewClient = WebViewClient()
        return v
    }

    private fun fetchBundle(arguments: Bundle?) {
        if (arguments != null) {
            url = arguments.getString(KET_URL)!!
        }
    }


}
