package com.farid.testappapi.ui.lists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.farid.testappapi.R
import com.farid.testappapi.ui.MainActivity
import com.farid.testappapi.utils.FragmentUtils
import kotlinx.android.synthetic.main.list_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment : Fragment() {

    companion object {
        var TAG = ListFragment::class.java.simpleName
    }

    private val listViewModel: ListViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listViewModel.getListAll(0).observe(this, Observer { it ->
            llLoading.visibility = View.GONE
            if (it.size > 0) {
                rvList.visibility = View.VISIBLE
                rvList.adapter =
                    ListAdapter(it, listViewModel, this@ListFragment, onClickWeb = { url ->
                        FragmentUtils.ReplaceFragment(
                            (activity as MainActivity),
                            WebViewFragment.newInstance(url),
                            true,
                            WebViewFragment.TAG
                        )
                    })
            } else {
            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}
