package com.farid.testappapi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.farid.testappapi.R
import com.farid.testappapi.ui.lists.ListFragment
import com.farid.testappapi.utils.FragmentUtils

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FragmentUtils.ReplaceFragment(this, ListFragment(), false, ListFragment.TAG)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
