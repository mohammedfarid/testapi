package com.farid.testappapi.ui.lists

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.farid.testappapi.R
import com.farid.testappapi.models.response.ListAllPublicResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_list.view.*

class ListAdapter(
    var listAllPublicResponse: ArrayList<ListAllPublicResponse>,
    val listViewModel: ListViewModel,
    var fragment: ListFragment,
    var onClickWeb: (String) -> Unit
) : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listAllPublicResponse.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(listAllPublicResponse[position])
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(listAllPublicResponse: ListAllPublicResponse) {
            Picasso.get().load(listAllPublicResponse.owner!!.avatarUrl).into(itemView.ivProfile)
            itemView.tvBody.text = listAllPublicResponse.name
            listViewModel.getRepoInfo(
                listAllPublicResponse.owner.login!!,
                listAllPublicResponse.name!!
            )
                .observe(fragment, Observer {
                    itemView.tvDesBody.text = it.description
                    itemView.tvCreateDateBody.text = it.createdAt
                    itemView.tvForksCountBody.text = it.forksCount.toString()
                    itemView.tvLanguageBody.text = it.language
                })

            itemView.setOnClickListener {
                onClickWeb(listAllPublicResponse.htmlUrl!!)
            }
        }
    }
}