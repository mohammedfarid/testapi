package com.farid.testappapi.ui.lists

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.farid.testappapi.models.response.ListAllPublicResponse
import com.farid.testappapi.models.response.infoRepo.InfoRepoResponse
import com.farid.testappapi.network.apiGitHub.ListApiRepository

class ListViewModel(val listApiRepository: ListApiRepository) : ViewModel() {
    fun getListAll(lastId: Int): LiveData<ArrayList<ListAllPublicResponse>> {
        return listApiRepository.getList(lastId)
    }
    fun getRepoInfo(owner: String,title:String):LiveData<InfoRepoResponse>{
        return listApiRepository.getRepo(owner,title)
    }
}
