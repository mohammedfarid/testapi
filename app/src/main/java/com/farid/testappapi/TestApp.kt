package com.farid.testappapi

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.farid.testappapi.di.apiModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class TestApp:Application() {
    companion object {

        fun hasNetwork(): Boolean {
            return instance!!.checkIfHasNetwork()
        }

        var instance: TestApp? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = getInstance()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@TestApp)
            androidFileProperties()
            modules(
                listOf(
                    apiModule
                )
            )
        }
    }

    private fun getInstance(): TestApp? {
        return instance
    }


    fun checkIfHasNetwork(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}